DROP TABLE cats;

CREATE TABLE cats (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255),
    image VARCHAR(255)
);

INSERT INTO cats (name, image)
VALUES 
("Tuna", 'https://images.unsplash.com/photo-1532971731140-1d7cccc06c3f?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MXx8c2lhbWVzZXxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60'),
("Marla", 'https://images.unsplash.com/photo-1509567406695-7d7fbc6d2ecb?ixid=MXwxMjA3fDB8MHxzZWFyY2h8Nnx8dGFiYnl8ZW58MHx8MHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60'),
("Sam", 'https://images.unsplash.com/photo-1595787664454-cb73fa2b5aea?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTR8fG1haW5lJTIwY29vbnxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60'),
("Cheeto", 'https://images.unsplash.com/photo-1463008420065-8274332e2be8?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MXx8bmFrZWQlMjBjYXR8ZW58MHx8MHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60'),
("Toby", 'https://images.unsplash.com/photo-1523863745117-a610a34eb231?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8c2lhbWVzZXxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60');

SELECT * FROM cats;
