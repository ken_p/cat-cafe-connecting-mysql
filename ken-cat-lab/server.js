import express from "express";
import db from  "./src/database/connection";
import cors from "cors";

const app = express();
app.use(cors());

//middleware
app.use(express.json());


app.get("/api", (req, res) => {
    res.send("hello world");
});

app.get("/api/cats", (req, res) => {
  db.query('SELECT * FROM cats', function (error, results, fields) {
    if (error) throw error;
    return res.status(200).send(results);
  });
});


const PORT = 3001;
app.listen(PORT, () => console.log(`server started on 3001...`));