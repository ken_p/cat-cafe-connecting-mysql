const mysql = require('mysql');

const connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'nodeuser5',
  password : 'MyPass123',
  database : 'cat_cafe_me'
});
 
connection.connect(function(err) {
    if (err) {
      console.error('error connecting: ' + err.stack);
      return;
    }
   
    console.log('database connected yup');
  });

  module.exports = connection;