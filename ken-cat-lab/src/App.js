import './App.css';
import CatProfile from "./CatProfile/CatProfile";

function App() {
  return (
    <div className="App">
     <CatProfile/>
    </div>
  );
}

export default App;
